<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="img/sbca.png">
    <title>{{ config('app.name', 'FPES') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    
    <!-- Custom styles for this template -->
    <link href="carousel.css" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="navbar-top-fixed.css" rel="stylesheet">
    <link href="../../dist/css/bootstrap.min.css" rel="stylesheet">

    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
    .back-to-top {
    cursor: pointer;
    position: fixed;
    bottom: 20px;
    right: 20px;
    display:none;
}

    </style>

</head>
<body id="page-top">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark fixed-top navbar-laravel" style = "background-color:maroon">
                <a class="navbar-brand js-scroll-trigger" href="/">
                    <img src = "/img/sbca.png" height = "50" style = "margin-bottom:2%">
                    <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">San Beda College Alabang</font>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                </button>
                {{-- {{Auth::logout()}} --}}
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
            
                    </ul>
            
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" style = "color:white;font-size:150%" href="/">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style = "color:white;font-size:150%" href="/">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style = "color:white;font-size:150%" href="/">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" style = "color:white;font-size:150%" href="/">Contact</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" style = "color:white;font-size:150%" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>
            
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" style = "color:maroon;font-size:150%" href="{{ route('logout') }}" data-toggle="modal" data-target="#logout">
                                        {{ __('Logout') }}
                                    </a>
            
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </nav>
            
            <!-- Logout Modal -->
            <div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="logout">Logout</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          Are you sure?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                          <button type="button" class="btn btn-danger" onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">Continue</button>
                        </div>
                      </div>
                    </div>
                  </div>
        <main class="py-4" >
                <div style = "position:fixed;z-index: 1;width:100%;text-align:center">
                    @include('inc.messages')
                </div>
                <div class="container" style = "margin-top:5%">
                        <div class="row justify-content-center">
                            <div class="col-md-8">
                                <div class="card">
                                    <div class="card-header">{{ __('Register') }}</div>
                    
                                    <div class="card-body">
                                            <form method="POST" action="{{ route('register') }}" class = "was-validated">
                    
                                                    @csrf
                                            
                                                    <div class="form-row">
                                            
                                                            <div class="form-group col-md-6">
                                                  
                                                                <label for="email">{{ __('E-Mail Address') }}</label>
                                                  
                                                                <input placeholder="john@sample.com" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                            
                                                                @if ($errors->has('email'))
                                                                    <span class="invalid-feedback">
                                                                        <strong>{{ $errors->first('email') }}</strong>
                                                                    </span>
                                                                @endif
                                            
                                                                <div class="valid-feedback">
                                                                        Looks good!
                                                                </div>
                                            
                                                                <div class="invalid-feedback">
                                                                        Please provide a valid email.
                                                                </div>
                                                  
                                                            </div>
                                                  
                                                            <div class="form-group col-md-6">
                                                  
                                                                    <label for="username">{{ __('Username') }}</label>
                                                  
                                                                    <input placeholder="johndoe" id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                                            
                                                                    @if ($errors->has('username'))
                                                                        <span class="invalid-feedback">
                                                                            <strong>{{ $errors->first('username') }}</strong>
                                                                        </span>
                                                                    @endif
                                            
                                                                    <div class="valid-feedback">
                                                                            Looks good!
                                                                    </div>
                                            
                                                                    <div class="invalid-feedback">
                                                                            Please provide a valid username.
                                                                    </div>
                                                  
                                                            </div>
                                                  
                                                          </div>
                                            
                                            
                                                          <div class="form-row">
                                            
                                                                <div class="form-group col-md-6">
                                                      
                                                                        <label for="password">{{ __('Password') }}</label>
                                                      
                                                                        <input placeholder="Password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                            
                                                                        @if ($errors->has('password'))
                                                                            <span class="invalid-feedback">
                                                                                <strong>{{ $errors->first('password') }}</strong>
                                                                            </span>
                                                                        @endif
                                            
                                                                        <div class="valid-feedback">
                                                                                Looks good!
                                                                        </div>
                                            
                                                                        <div class="invalid-feedback">
                                                                                Please provide a valid password.
                                                                        </div>
                                                      
                                                                </div>
                                                      
                                                                <div class="form-group col-md-6">
                                                      
                                                                        <label for="password-confirm">{{ __('Confirm Password') }}</label>
                                                      
                                                                        <input placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                            
                                                                            <div class="valid-feedback">
                                                                                    Looks good!
                                                                            </div>
                                                
                                                                            <div class="invalid-feedback">
                                                                                    Please confirm your password.
                                                                            </div>
                                                      
                                                                </div>
                                                      
                                                              </div>
                    
                                                              <div class="form-row">
                                            
                                                                    <div class="form-group col-md-6">
                                                          
                                                                            <select class="custom-select" name = "role" required>
                                                                                    <option value="">Choose...</option>
                                                                                    <option value="student">Student</option>
                                                                                    <option value="professor">Professor</option>
                                                                                  </select>
                                            
                                                                                  <div class="valid-feedback">
                                                                                        Looks good!
                                                                                </div>
                                                
                                                                                <div class="invalid-feedback">
                                                                                        Please indicate your position.
                                                                                </div>
                                                          
                                                                    </div>
                                                          
                                                                    <div class="form-group col-md-6">
                                                          
                                                                            <div class="form-check">
                                                                                    <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" required>
                                                                                    <label class="form-check-label" for="invalidCheck3">
                                                                                      Agree to terms and conditions
                                                                                    </label>
                                                                                  </div>
                                                          
                                                                    </div>
                                                          
                                                                  </div>
                                            
                                                            <div class="form-group row mb-0">
                                                                <div class="col-md-6 offset-md-0">
                                                                    <button type="submit" class="btn btn-primary">
                                                                        {{ __('Register') }}
                                                                    </button>
                                                                </div>
                                                            </div>
                                                  </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </main>
        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Click to return on the top page" data-toggle="tooltip" data-placement="left"><i class="fas fa-chevron-up"></i></a>
        <hr class="featurette-divider">
        @include('inc.footer')
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>

            <!-- Scripts -->
    <script src='/js/app.js'></script>

</body>
</html>
