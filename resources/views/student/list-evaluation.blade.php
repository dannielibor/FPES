@extends('layouts.student')
<style>
        .user-subhead {
            font-size: 0.875em;
            font-style: italic;
        }
        .user-link {
            display: block;
            font-size: 1.25em;
            padding-top: 3px;
            margin-left: 60px;
        }
        .user-list{
            position: relative;
            max-width: 50px;
            float: left;
            margin-right: 15px;
        }
        .table tbody tr td {
            font-size: 0.875em;
            vertical-align: middle;
            border-top: 1px solid #e7ebee;
            padding: 12px 8px;
        }
        #imageUpload
            {
                display: none;
            }
            
            #profileImage
            {
                cursor: pointer;
            }
            
            #profile-container {
                width: 150px;
                height: 150px;
                overflow: hidden;
                -webkit-border-radius: 50%;
                -moz-border-radius: 50%;
                -ms-border-radius: 50%;
                -o-border-radius: 50%;
                border-radius: 50%;
            }
            
            #profile-container img {
                width: 150px;
                height: 150px;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@section('content')
<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Student</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark" style = "background-color:maroon">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b style = "margin-right:3%">
                            <img src = "/img/sbca.png" height = "50">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                        <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Student</font></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)" style = "color:white"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" style = "color:white"><i class="fa fa-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-light" href="/student-profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/storage/profile_img/{{$student->img}}" alt="user" class="img-circle" width="30"></a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src = "/img/sbca.png" height = "50"></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)" style= "color:white"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)" style = "color:red"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/student-dashboard" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/student-profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/student-list" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Evaluation</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="#" data-toggle="modal" data-target="#logout" aria-expanded="false"><i class="fas fa-sign-out-alt"></i><span class="hide-menu"></span>{{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </ul>
                </nav>
               
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
                @include('inc.messages')
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Table<a href = "users" name = "refresh" class = "btn btn-primary" style = "margin-left:3%"><i class="fas fa-sync"></i></a></h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Professors</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">List</h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th style = "text-align:center">#</th>
                                                <th>Professor</th>
                                                <th style = "text-align:center">Status</th>
                                                <th style = "text-align:center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($profs) > 0)
                                            @foreach($profs as $prof)
                                            <tr>
                                            
                                                <td style = "text-align:center">{{$prof->users->id}}</td>

                                                @if($prof->users->firstname == '' && $prof->users->lastname != '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$prof->users->img}}" alt="">
                                                    <a href="/users/{{$prof->id}}" class="user-link">- {{$prof->users->lastname}}</a>
                                                    <span class="user-subhead">{{$prof->schedules->subject_title}}</span>
                                                </td>
                                                @elseif($prof->users->lastname == '' && $prof->users->firstname != '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$prof->users->img}}" alt="">
                                                    <a href="/users/{{$prof->users->id}}" class="user-link">{{$prof->users->firstname}} -</a>
                                                    <span class="user-subhead">{{$prof->schedules->subject_title}}</span>
                                                </td>
                                                @elseif($prof->users->lastname == '' && $prof->users->firstname == '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$prof->users->img}}" alt="" class = "user-list">
                                                    <a href="/users/{{$prof->users->id}}" class="user-link">unknown</a>
                                                    <span class="user-subhead">{{$prof->schedules->subject_title}}</span>
                                                </td>
                                                @else
                                                <td>
                                                    <img src="/storage/profile_img/{{$prof->users->img}}" alt="" class = "user-list">
                                                    <a href="/users/{{$prof->id}}" class="user-link">{{$prof->users->firstname}} {{$prof->users->lastname}}</a>
                                                    <span class="user-subhead">{{$prof->schedules->subject_title}}</span>
                                                </td>
                                                @endif

                                                @if($prof->status == 'pending')
                                                <td style = "text-align:center"><span class="label label-warning">{{$prof->status}}</span> </td>
                                                @else
                                                <td style = "text-align:center"><span class="label label-success">{{$prof->status}}</span> </td>
                                                @endif

                                                <td style = "text-align:center">
                                        
                                                    <a href="/users/{{$prof->id}}" class="table-link">
                                                            <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    </a>

                                                </td>
                                            </tr>
                                            @endforeach
                                            <div style = "float:right">
                                                {{-- {{$profs->links()}} --}}
                                            </div>
                                            @else
                                            <tr>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Elegent Admin by wrappixel.com
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
     <!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>

      <script>
          $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
            $("#profileImage").click(function(e) {
                $("#imageUpload").click();
            });
            
            function fasterPreview( uploader ) {
                if ( uploader.files && uploader.files[0] ){
                      $('#profileImage').attr('src', 
                         window.URL.createObjectURL(uploader.files[0]) );
                }
            }
            
            $("#imageUpload").change(function(){
                fasterPreview( this );
            });
            </script>

@endsection