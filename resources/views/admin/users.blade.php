@extends('layouts.admin')
<style>
    .user-subhead {
        font-size: 0.875em;
        font-style: italic;
    }
    .user-link {
        display: block;
        font-size: 1.25em;
        padding-top: 3px;
        margin-left: 60px;
    }
    .user-list{
        position: relative;
        max-width: 50px;
        float: left;
        margin-right: 15px;
    }
    .table tbody tr td {
        font-size: 0.875em;
        vertical-align: middle;
        border-top: 1px solid #e7ebee;
        padding: 12px 8px;
    }
    #imageUpload
        {
            display: none;
        }
        
        #profileImage
        {
            cursor: pointer;
        }
        
        #profile-container {
            width: 150px;
            height: 150px;
            overflow: hidden;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            -o-border-radius: 50%;
            border-radius: 50%;
        }
        
        #profile-container img {
            width: 150px;
            height: 150px;
        }

        .span_pseudo, .chiller_cb span:before, .chiller_cb span:after {
  content: "";
  display: inline-block;
  background: #fff;
  width: 0;
  height: 0.2rem;
  position: absolute;
  transform-origin: 0% 0%;
}

.chiller_cb {
  position: relative;
  height: 2rem;
  display: flex;
  align-items: center;
}
.chiller_cb input {
  display: none;
}
.chiller_cb input:checked ~ span {
  background: #fd2727;
  border-color: #fd2727;
}
.chiller_cb input:checked ~ span:before {
  width: 1rem;
  height: 0.15rem;
  transition: width 0.1s;
  transition-delay: 0.3s;
}
.chiller_cb input:checked ~ span:after {
  width: 0.4rem;
  height: 0.15rem;
  transition: width 0.1s;
  transition-delay: 0.2s;
}
.chiller_cb input:disabled ~ span {
  background: #ececec;
  border-color: #dcdcdc;
}
.chiller_cb input:disabled ~ label {
  color: #dcdcdc;
}
.chiller_cb input:disabled ~ label:hover {
  cursor: default;
}
.chiller_cb label {
  padding-left: 2rem;
  position: relative;
  z-index: 2;
  cursor: pointer;
  margin-bottom:0;
}
.chiller_cb span {
  display: inline-block;
  width: 1.2rem;
  height: 1.2rem;
  border: 2px solid #ccc;
  position: absolute;
  left: 0;
  transition: all 0.2s;
  z-index: 1;
  box-sizing: content-box;
}
.chiller_cb span:before {
  transform: rotate(-55deg);
  top: 1rem;
  left: 0.37rem;
}
.chiller_cb span:after {
  transform: rotate(35deg);
  bottom: 0.35rem;
  left: 0.2rem;
}
    </style>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@section('content')
<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Portal</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark" style = "background-color:maroon">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b style = "margin-right:3%">
                            <img src = "/img/sbca.png" height = "50">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                        <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Admin Portal</font></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)" style = "color:white"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" style = "color:white"><i class="fa fa-search"></i></a>
                            <form class="app-search" method = "get" action = "/search">
                                <input type="text" class="form-control" placeholder="Search &amp; enter" name = "search"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-light" href="/pages-profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/storage/profile_img/{{$admin[0]->img}}" alt="user" class="img-circle" width="30"></a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src = "/img/sbca.png" height = "50"></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)" style= "color:white"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)" style = "color:red"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="{{ action("AdminController@index") }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/pages-profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/users" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Tables</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/admins" aria-expanded="false"><i class="fas fa-user-tie"></i><span class="hide-menu"></span>Administrators</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="icon-fontawesome.html" aria-expanded="false"><i class="fas fa-list-ul"></i><span class="hide-menu"></span>Forms</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="fas fa-envelope"></i><span class="hide-menu"></span>Messages</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="fas fa-cog"></i><span class="hide-menu"></span>Settings</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/trash" aria-expanded="false"><i class="fas fa-trash"></i><span class="hide-menu"></span>Trash</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="#" data-toggle="modal" data-target="#logout" aria-expanded="false"><i class="fas fa-sign-out-alt"></i><span class="hide-menu"></span>{{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
                @include('inc.messages')
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Table<a href = "users" name = "refresh" class = "btn btn-info" style = "margin-left:3%"><i class="fas fa-sync"></i></a></h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Table</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <form action = "/deleteAll" method = "post">
                    {!! csrf_field() !!}
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Accounts 
                                    <a href = "#" data-toggle="modal" data-target="#register" class = "btn btn-info" style = "margin-left:3%">New</a>
                                    <a href = "#" data-toggle="modal" data-target="#deleteAll" class = "btn btn-danger" style = "margin-left:1%">Delete</a>
                                    
                                    <!-- Delete Modal -->
                                    <div class="modal fade" id="deleteAll" tabindex="-1" role="dialog" aria-labelledby="deleteAll" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteAll">Delete</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type = "submit" class = "btn btn-danger">Delete</button>
                                                </div>
                                                </div>
                                            </div>
                                    </div>

                                </h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width = "1%" style = "text-align:center">#</th>
                                                <th>Users</th>
                                                <th style = "text-align:center">Created</th>
                                                <th style = "text-align:center">Status</th>
                                                <th style = "text-align:center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($users) > 0)
                                            @foreach($users as $user)
                                            <tr>
                                                <td class="chiller_cb" id = "checkbox" style = "text-align:center;padding-top:40px">
                                                    <input id = "myCheckbox{{$user->id}}" type = "checkbox" name = "delete[]" value = "{{$user->id}}">
                                                    <label for = "myCheckbox{{$user->id}}">{{$user->id}}</label>
                                                    <span></span>
                                                </td>

                                                @if($user->firstname == '' && $user->lastname != '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$user->img}}" alt="">
                                                    <a href="/users/{{$user->id}}" class="user-link">- {{$user->lastname}}</a>
                                                    <span class="user-subhead">{{$user->role}}</span>
                                                </td>
                                                @elseif($user->lastname == '' && $user->firstname != '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$user->img}}" alt="">
                                                    <a href="/users/{{$user->id}}" class="user-link">{{$user->firstname}} -</a>
                                                    <span class="user-subhead">{{$user->role}}</span>
                                                </td>
                                                @elseif($user->lastname == '' && $user->firstname == '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$user->img}}" alt="" class = "user-list">
                                                    <a href="/users/{{$user->id}}" class="user-link">unknown</a>
                                                    <span class="user-subhead">{{$user->role}}</span>
                                                </td>
                                                @else
                                                <td>
                                                    <img src="/storage/profile_img/{{$user->img}}" alt="" class = "user-list">
                                                    <a href="/users/{{$user->id}}" class="user-link">{{$user->firstname}} {{$user->lastname}}</a>
                                                    <span class="user-subhead">{{$user->role}}</span>
                                                </td>
                                                @endif
                                                
                                                <td style = "text-align:center">{{$user->created_at}}</td>

                                                @if($user->status == 'pending')
                                                <td style = "text-align:center"><span class="label label-warning">{{$user->status}}</span> </td>
                                                @else
                                                <td style = "text-align:center"><span class="label label-success">{{$user->status}}</span> </td>
                                                @endif

                                                <td style = "text-align:center">
                                        
                                                    <a href="/users/{{$user->id}}" class="table-link">
                                                            <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    </a>
                                                    
                                                    <a href="" class="table-link" style = "color: #e74c3c" data-toggle = "modal" data-target = "#delete{{$user->id}}">
                                                    <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    </a>
                                                            <!-- Delete Modal -->
                                                            <div class="modal fade" id="delete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="delete{{$user->id}}" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="delete{{$user->id}}">Delete</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            Are you sure?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                            <a href = "/delete/{{$user->id}}" class="btn btn-danger">Delete</a>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                            </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            <div style = "float:right">
                                                {{$users->links()}}
                                            </div>
                                            @else
                                            <tr>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                <!-- Register Modal -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
                <form method="POST" action="/create" class = "was-validated" enctype="multipart/form-data">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="register">Register</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                    
    
                            @csrf

                        <div class="form-row">

                            <div class="form-group">

                                <div id="profile-container">
                                    <image id="profileImage" src="/storage/profile_img/default.jpg" data-toggle="tooltip" data-placement="top" title="Browse"/>
                                </div>
                                    <input id="imageUpload" type="file" name="profile_img" placeholder="Photo" capture>
                            
                            </div>

                            <div class="form-group col-md-8">
                          
                                    <label for="firstname">{{ __('Firstname') }}</label>
                  
                                    <input placeholder="john" id="firstname" type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" autofocus>
                                    <br><br>
                                    <label for="lastname">{{ __('Lastname') }}</label>
                  
                                    <input placeholder="doe" id="lastname" type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" autofocus>
                           
                            </div>

                        </div>
                    
                            <div class="form-row">

                                    <div class="form-group col-md-6">
                          
                                        <label for="email">{{ __('E-Mail Address') }}</label>
                          
                                        <input placeholder="john@sample.com" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                    
                                        <div class="valid-feedback">
                                                Looks good!
                                        </div>
                    
                                        <div class="invalid-feedback">
                                                Please provide a valid email.
                                        </div>
                          
                                    </div>
                          
                                    <div class="form-group col-md-6">
                          
                                            <label for="username">{{ __('Username') }}</label>
                          
                                            <input placeholder="johndoe" id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                    
                                            @if ($errors->has('username'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                            @endif
                    
                                            <div class="valid-feedback">
                                                    Looks good!
                                            </div>
                    
                                            <div class="invalid-feedback">
                                                    Please provide a valid username.
                                            </div>
                          
                                    </div>
                          
                                  </div>
                    
                    
                                  <div class="form-row">
                    
                                        <div class="form-group col-md-6">
                              
                                                <label for="password">{{ __('Password') }}</label>
                              
                                                <input placeholder="Password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                    
                                                <div class="valid-feedback">
                                                        Looks good!
                                                </div>
                    
                                                <div class="invalid-feedback">
                                                        Please provide a valid password.
                                                </div>
                              
                                        </div>
                              
                                        <div class="form-group col-md-6">
                              
                                                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                              
                                                <input placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    
                                                    <div class="valid-feedback">
                                                            Looks good!
                                                    </div>
                        
                                                    <div class="invalid-feedback">
                                                            Please confirm your password.
                                                    </div>
                              
                                        </div>
                              
                                      </div>
    
                                      <div class="form-row">
                    
                                            <div class="form-group col-md-6">

                                                    <label for="studNum">{{ __('Role') }}</label>
                                  
                                                    <select class="custom-select" name = "role" id = "select" onchange="val()" required>
                                                            <option value="">Choose...</option>
                                                            <option value="student">Student</option>
                                                            <option value="professor">Professor</option>
                                                          </select>
                    
                                                          <div class="valid-feedback">
                                                                Looks good!
                                                        </div>
                        
                                                        <div class="invalid-feedback">
                                                                Please indicate your position.
                                                        </div>
                                  
                                            </div>

                                            <div class="form-group col-md-6" style = "display:none" id = "student">
                          
                                                    <label for="studNum">{{ __('Student ID') }}</label>
                                  
                                                    <input placeholder="##########" id="studNum" type="text" class="form-control" name="studNum" value="{{ old('studNum') }}" autofocus>
                                  
                                            </div>

                                            <div class="form-group col-md-6" style = "display:none" id = "professor">
                          
                                                    <label for="profNum">{{ __('Professor ID') }}</label>
                                  
                                                    <input placeholder="##########" id="profNum" type="text" class="form-control" name="profNum" value="{{ old('profNum') }}" autofocus>
                                  
                                            </div>
    
                                        </div>
                                          <div class="form-group col-md-6">
                                  
                                                <div class="form-check">
                                                        <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" required>
                                                        <label class="form-check-label" for="invalidCheck3">
                                                          Agree to terms and conditions
                                                        </label>
                                                      </div>
                              
                                        </div>
    
                                          
                         
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">{{ __('Register') }}</button>
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </form>
          </div>
        </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <div class="container-fluid">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- End Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Certificate Of Registration
                                        <a href = "#" data-toggle="modal" data-target="#import" class = "btn btn-warning" style = "margin-left:3%">Import <i class="fas fa-upload"></i></a>
                                    </h4>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th style = "text-align:center">Reg.#</th>
                                                    <th style = "text-align:center">Name</th>
                                                    <th style = "text-align:center">Student ID</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if(count($cors) > 0)
                                                @foreach($cors as $cor)
                                                <tr>    
                                                    <td style = "text-align:center">{{$cor->regId}}</td>
                                                    
                                                    <td style = "text-align:center">{{$cor->lastname}}, {{$cor->firstname}} </td>
                                                    
                                                    <td style = "text-align:center">{{$cor->student_id}}</td>
                                                </tr>
                                                @endforeach
                                                <div class = "float:right">
                                                    {{$cors->links()}}
                                                </div>
                                                @else
                                                <tr>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                <td>-</td>
                                                </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End PAge Content -->
                    <!-- ============================================================== -->
                </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Elegent Admin by wrappixel.com
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

                    <!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>

    <!-- Import Modal -->
    <div class="modal fade" id="import" tabindex="-1" role="dialog" aria-labelledby="import" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                    <form method="POST" action="/import" class = "was-validated" enctype="multipart/form-data">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="import">Import</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                        
        
                                @csrf
    
                            <div class="form-row">
    
                                <div class="form-group">
                      
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="file-upload" name = "file" accept = ".xlsx" required>
                                            <label class="custom-file-label" for="validatedCustomFile" id="file-upload-filename">Choose file...</label>
                                        </div>
                      
                                </div>
    
                            </div>
                             
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">{{ __('Import') }}</button>
                </div>
            </form>
              </div>
            </div>

<script>
    var input = document.getElementById( 'file-upload' );
var infoArea = document.getElementById( 'file-upload-filename' );

input.addEventListener( 'change', showFileName );

function showFileName( event ) {
  
  // the change event gives us the input it occurred in 
  var input = event.srcElement;
  
  // the input has an array of files in the `files` property, each one has a name that you can use. We're just using the name here.
  var fileName = input.files[0].name;
  
  // use fileName however fits your app best, i.e. add it into a div
  infoArea.textContent = fileName;
}
    function val() {
        var sel = document.getElementById("select");
        var value = sel.options[sel.selectedIndex].value;
        var student = document.getElementById('student');
        var professor = document.getElementById('professor');
            if (value == 'student'){
                student.style.display = 'block';
                professor.style.display = 'none';
            }
            else if (value == 'professor'){
                student.style.display = 'none';
                professor.style.display = 'block';
            }else{
                student.style.display = 'none';
                professor.style.display = 'none';
            }
    }
    $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
            $("#profileImage").click(function(e) {
                $("#imageUpload").click();
            });
            
            function fasterPreview( uploader ) {
                if ( uploader.files && uploader.files[0] ){
                      $('#profileImage').attr('src', 
                         window.URL.createObjectURL(uploader.files[0]) );
                }
            }
            
            $("#imageUpload").change(function(){
                fasterPreview( this );
            });

</script>

    @endsection