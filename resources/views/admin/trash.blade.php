@extends('layouts.admin')
<style>
    .user-subhead {
        font-size: 0.875em;
        font-style: italic;
    }
    .user-link {
        display: block;
        font-size: 1.25em;
        padding-top: 3px;
        margin-left: 60px;
    }
    .user-list{
        position: relative;
        max-width: 50px;
        float: left;
        margin-right: 15px;
    }
    .table tbody tr td {
        font-size: 0.875em;
        vertical-align: middle;
        border-top: 1px solid #e7ebee;
        padding: 12px 8px;
    }
    </style>
@section('content')
<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Portal</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark" style = "background-color:maroon">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b style = "margin-right:3%">
                            <img src = "/img/sbca.png" height = "50">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                        <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Admin Portal</font></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)" style = "color:white"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" style = "color:white"><i class="fa fa-search"></i></a>
                            <form class="app-search" method = "get" action = "/searchTrash">
                                <input type="text" class="form-control" placeholder="Search &amp; enter" name = "search"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a href="/pages-profile" class="nav-link dropdown-toggle text-muted waves-effect waves-light" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/storage/profile_img/{{$admin[0]->img}}" alt="user" class="img-circle" width="30"></a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src = "/img/sbca.png" height = "50"></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)" style= "color:white"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)" style = "color:red"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="{{ action("AdminController@index") }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/pages-profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/users" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Tables</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/admins" aria-expanded="false"><i class="fas fa-user-tie"></i><span class="hide-menu"></span>Administrators</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="icon-fontawesome.html" aria-expanded="false"><i class="fas fa-list-ul"></i><span class="hide-menu"></span>Forms</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="fas fa-envelope"></i><span class="hide-menu"></span>Messages</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="fas fa-cog"></i><span class="hide-menu"></span>Settings</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/trash" aria-expanded="false"><i class="fas fa-trash"></i><span class="hide-menu"></span>Trash</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="#" data-toggle="modal" data-target="#logout" aria-expanded="false"><i class="fas fa-sign-out-alt"></i><span class="hide-menu"></span>{{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
                @include('inc.messages')
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Trash<a href = "trash" name = "refresh" class = "btn btn-primary" style = "margin-left:3%"><i class="fas fa-sync"></i></a></h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Trash</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <form action = "/trashAll" method = "post">
                    {!! csrf_field() !!}
                <div class="row">
                    <!-- column -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Accounts
                                    <a href = "#" data-toggle="modal" data-target="#recoverAll" class = "btn btn-primary" style = "margin-left:3%"><i class="fas fa-undo"></i></a>
                                    <a href = "#" data-toggle="modal" data-target="#deleteAll" class = "btn btn-danger" style = "margin-left:1%"><i class="fas fa-minus"></i></a>

                                    <!-- Delete Modal -->
                                    <div class="modal fade" id="deleteAll" tabindex="-1" role="dialog" aria-labelledby="deleteAll" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteAll">Delete</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    Are you sure?
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                    <button type = "submit" class = "btn btn-danger" name = "submit" value = "delete">Delete</button>
                                                </div>
                                                </div>
                                            </div>
                                    </div>

                                    <!-- Recover Modal -->
                                    <div class="modal fade" id="recoverAll" tabindex="-1" role="dialog" aria-labelledby="recoverAll" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="recoverAll">Delete</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                <button type = "submit" class = "btn btn-primary" name = "submit" value = "recover">Recover</button>
                                            </div>
                                            </div>
                                        </div>
                                </div>

                                </h4>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width = "1%" style = "text-align:center">Select</th>
                                                <th style = "text-align:center">#</th>
                                                <th>Users</th>
                                                <th style = "text-align:center">Created</th>
                                                <th style = "text-align:center">Status</th>
                                                <th style = "text-align:center"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($bin) > 0)
                                            @foreach($bin as $bins)
                                            <tr>
                                                <td id = "checkbox" style = "text-align:center"><input type = "checkbox" name = "delete[]" value = "{{$bins->userId}}"></td>
                                                <td style = "text-align:center">{{$bins->userId}}</td>

                                                @if($bins->firstname == '' && $bins->lastname != '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$bins->img}}" alt="">
                                                    <a href = "/view/{{$bins->userId}}" class="user-link">- {{$bins->lastname}}</a>
                                                    <span class="user-subhead">{{$bins->role}}</span>
                                                </td>
                                                @elseif($bins->lastname == '' && $bins->firstname != '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$bins->img}}" alt="">
                                                    <a href = "/view/{{$bins->userId}}" class="user-link">{{$bins->firstname}} -</a>
                                                    <span class="user-subhead">{{$bins->role}}</span>
                                                </td>
                                                @elseif($bins->lastname == '' && $bins->firstname == '')
                                                <td>
                                                    <img src="/storage/profile_img/{{$bins->img}}" alt="" class = "user-list">
                                                    <a href = "/view/{{$bins->userId}}" class="user-link">unknown</a>
                                                    <span class="user-subhead">{{$bins->role}}</span>
                                                </td>
                                                @else
                                                <td>
                                                    <img src="/storage/profile_img/{{$bins->img}}" alt="" class = "user-list">
                                                    <a href = "/view/{{$bins->userId}}" class="user-link">{{$bins->firstname}} {{$bins->lastname}}</a>
                                                    <span class="user-subhead">{{$bins->role}}</span>
                                                </td>
                                                @endif
                                                
                                                <td style = "text-align:center">{{$bins->created_at}}</td>

                                                @if($bins->status == 'pending')
                                                <td style = "text-align:center"><span class="label label-warning">{{$bins->status}}</span> </td>
                                                @else
                                                <td style = "text-align:center"><span class="label label-success">{{$bins->status}}</span> </td>
                                                @endif

                                                <td style = "text-align:center">
                                        
                                                    <a href="/view/{{$bins->userId}}" class="table-link">
                                                            <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fas fa-search-plus fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    </a>
                                                    
                                                    <a href="" class="table-link" style = "color: #e74c3c" data-toggle = "modal" data-target = "#delete{{$bins->userId}}">
                                                    <span class="fa-stack">
                                                        <i class="fa fa-square fa-stack-2x"></i>
                                                        <i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                                                    </span>
                                                    </a>
                                                            <!-- Delete Modal -->
                                                            <div class="modal fade" id="delete{{$bins->userId}}" tabindex="-1" role="dialog" aria-labelledby="delete{{$bins->userId}}" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <h5 class="modal-title" id="delete{{$bins->userId}}">Delete</h5>
                                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                            <span aria-hidden="true">&times;</span>
                                                                            </button>
                                                                        </div>
                                                                        <div class="modal-body">
                                                                            Are you sure?
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                                            <a href = "/deleteTrash/{{$bins->userId}}" class="btn btn-danger">Delete</a>
                                                                        </div>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                            <div style = "float:right">
                                                {{$bin->links()}}
                                            </div>
                                            @else
                                            <tr>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Elegent Admin by wrappixel.com
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

                    <!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>

@endsection