<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" sizes="16x16" href="img/sbca.png">
    <title>FPES - Admin Portal</title>
    <!-- This page CSS -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- chartist CSS -->
    <link href="../admin-dashboard/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--c3 plugins CSS -->
    <link href="../admin-dashboard/assets/node_modules/c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="../admin-dashboard/main/dist/css/style.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="../admin-dashboard/main/dist/css/pages/dashboard1.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style type="text/css">

    .cf:after { visibility: hidden; display: block; font-size: 0; content: " "; clear: both; height: 0; }
    * html .cf { zoom: 1; }
    *:first-child+html .cf { zoom: 1; }
    
    h1 { font-size: 1.75em; margin: 0 0 0.6em 0; }
    
    a { color: #2996cc; }
    a:hover { text-decoration: none; }
    
    p { line-height: 1.5em; }
    .small { color: #666; font-size: 0.875em; }
    .large { font-size: 1.25em; }
    
    /**
     * Nestable
     */
    
    .dd { position: relative; display: block; margin: 0; padding: 0; max-width: 600px; list-style: none; font-size: 13px; line-height: 20px; }
    
    .dd-list { display: block; position: relative; margin: 0; padding: 0; list-style: none; }
    .dd-list .dd-list { padding-left: 30px; }
    .dd-collapsed .dd-list { display: none; }
    
    .dd-item,
    .dd-empty,
    .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }
    
    .dd-handle { display: block; height: 30px; margin: 5px 0; padding: 5px 10px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
                border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd-handle:hover { color: #2ea8e5; background: #fff; }
    
    .dd-item > button { display: block; position: relative; cursor: pointer; float: left; width: 25px; height: 20px; margin: 5px 0; padding: 0; text-indent: 100%; white-space: nowrap; overflow: hidden; border: 0; background: transparent; font-size: 12px; line-height: 1; text-align: center; font-weight: bold; }
    .dd-item > button:before { content: '+'; display: block; position: absolute; width: 100%; text-align: center; text-indent: 0; }
    .dd-item > button[data-action="collapse"]:before { content: '-'; }
    
    .dd-placeholder,
    .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f2fbff; border: 1px dashed #b6bcbf; box-sizing: border-box; -moz-box-sizing: border-box; }
    .dd-empty { border: 1px dashed #bbb; min-height: 100px; background-color: #e5e5e5;
        background-image: -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                          -webkit-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image:    -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                             -moz-linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-image:         linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff),
                                  linear-gradient(45deg, #fff 25%, transparent 25%, transparent 75%, #fff 75%, #fff);
        background-size: 60px 60px;
        background-position: 0 0, 30px 30px;
    }
    
    .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
    .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
    .dd-dragel .dd-handle {
        -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
                box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
    }
    
    /**
     * Nestable Extras
     */
    
    .nestable-lists { display: block; clear: both; padding: 30px 0; width: 100%; border: 0; border-top: 2px solid #ddd; border-bottom: 2px solid #ddd; }
    
    #nestable-menu { padding: 0; margin: 20px 0; }
    
    #nestable-output,
    #nestable2-output { width: 100%; height: 7em; font-size: 0.75em; line-height: 1.333333em; font-family: Consolas, monospace; padding: 5px; box-sizing: border-box; -moz-box-sizing: border-box; }
    
    #nestable2 .dd-handle {
        color: #fff;
        border: 1px solid #999;
        background: #bbb;
        background: -webkit-linear-gradient(top, #bbb 0%, #999 100%);
        background:    -moz-linear-gradient(top, #bbb 0%, #999 100%);
        background:         linear-gradient(top, #bbb 0%, #999 100%);
    }
    #nestable2 .dd-handle:hover { background: #bbb; }
    #nestable2 .dd-item > button:before { color: #fff; }
    
    @media only screen and (min-width: 700px) {
    
        .dd { float: left; width: 48%; }
        .dd + .dd { margin-left: 2%; }
    
    }
    
    .dd-hover > .dd-handle { background: #2ea8e5 !important; }
    
    /**
     * Nestable Draggable Handles
     */
    
    .dd3-content { display: block; height: 30px; margin: 5px 0; padding: 5px 10px 5px 40px; color: #333; text-decoration: none; font-weight: bold; border: 1px solid #ccc;
        background: #fafafa;
        background: -webkit-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:    -moz-linear-gradient(top, #fafafa 0%, #eee 100%);
        background:         linear-gradient(top, #fafafa 0%, #eee 100%);
        -webkit-border-radius: 3px;
                border-radius: 3px;
        box-sizing: border-box; -moz-box-sizing: border-box;
    }
    .dd3-content:hover { color: #2ea8e5; background: #fff; }
    
    .dd-dragel > .dd3-item > .dd3-content { margin: 0; }
    
    .dd3-item > button { margin-left: 30px; }
    
    .dd3-handle { position: absolute; margin: 0; left: 0; top: 0; cursor: pointer; width: 30px; text-indent: 100%; white-space: nowrap; overflow: hidden;
        border: 1px solid #aaa;
        background: #ddd;
        background: -webkit-linear-gradient(top, #ddd 0%, #bbb 100%);
        background:    -moz-linear-gradient(top, #ddd 0%, #bbb 100%);
        background:         linear-gradient(top, #ddd 0%, #bbb 100%);
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .dd3-handle:before { content: '≡'; display: block; position: absolute; left: 0; top: 3px; width: 100%; text-align: center; text-indent: 0; color: #fff; font-size: 20px; font-weight: normal; }
    .dd3-handle:hover { background: #ddd; }
    
    /**
     * Socialite
     */
    
    .socialite { display: block; float: left; height: 35px; }
    
        </style>
</head>

<body class="skin-default-dark fixed-layout">
    
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Portal</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark" style = "background-color:maroon">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b style = "margin-right:3%">
                            <img src = "/img/sbca.png" height = "50">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                        <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Admin Portal</font></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)" style = "color:white"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" style = "color:white"><i class="fa fa-search"></i></a>
                            <form class="app-search" method = "get" action = "/search">
                                <input type="text" class="form-control" placeholder="Search &amp; enter" name = "search"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-light" href="/pages-profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/storage/profile_img/{{$admin[0]->img}}" alt="user" class="img-circle" width="30"></a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src = "/img/sbca.png" height = "50"></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)" style= "color:white"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)" style = "color:red"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="{{ action("AdminController@index") }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/pages-profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/users" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Tables</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/admins" aria-expanded="false"><i class="fas fa-user-tie"></i><span class="hide-menu"></span>Administrators</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="icon-fontawesome.html" aria-expanded="false"><i class="fas fa-list-ul"></i><span class="hide-menu"></span>Forms</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="fas fa-envelope"></i><span class="hide-menu"></span>Messages</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="fas fa-cog"></i><span class="hide-menu"></span>Settings</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/trash" aria-expanded="false"><i class="fas fa-trash"></i><span class="hide-menu"></span>Trash</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="#" data-toggle="modal" data-target="#logout" aria-expanded="false"><i class="fas fa-sign-out-alt"></i><span class="hide-menu"></span>{{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
                @include('inc.messages')
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid" style = "margin-top:2%">
                    <!-- ============================================================== -->
                    <!-- Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <!-- End Bread crumb and right sidebar toggle -->
                    <!-- ============================================================== -->
                
                    <!-- ============================================================== -->
                    <!-- Start Page Content -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <!-- column -->
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <menu id="nestable-menu">
                                    <h4 class="card-title">Programs
                                        <button type="button" data-action="expand-all" class = "btn btn-light"><i data-action="expand-all" class="fas fa-search-plus"></i></button>
                                        <button type="button" data-action="collapse-all" class = "btn btn-light"><i data-action="collapse-all" class="fas fa-search-minus"></i></button>
                                    </h4>
                                    <form method = "get" action = "/department">
                                        <input type="hidden" id = "val" value = "{{$departments}}">
                                        <div class ='form-group'>
                                            <input type="text" placeholder="Add department" name = "department" id = "department" class = "form-control col-md-2">
                                            <input type="submit" class = "btn btn-success" value = "Create">
                                        </div>
                                    </form>
                                    </menu>
                                    <div class="table-responsive">

                                            <form action="/departmentsubmit" method = "get">

                                                <div class = "container">

                                                    <ul class="list-group" id = "one">
                                                        @if(count($departments) > 0)
                                                        @foreach($departments as $department)
                                                        <li class="list-group" id = "two">

                                                            <div class="input-group mb-3">

                                                                <div class="input-group-prepend">

                                                                    <a href = "#" onclick = "add();" class = "btn btn-success input-group-text"><i class="fas fa-plus-circle"></i></a>
                                                                
                                                                </div>

                                                                <input type="text" class="form-control" value = "{{$department->department}}">

                                                            </div>
                                                            
                                                            <ul id = "three">
                                                            </ul>

                                                        </li>
                                                        @endforeach
                                                        @endif
                                                    </ul>

                                                </div>

                                            </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- End PAge Content -->
                    <!-- ============================================================== -->
                </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Elegent Admin by wrappixel.com
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->

                    <!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>


    <script src="../admin-dashboard/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap popper Core JavaScript -->
    <script src="../admin-dashboard/assets/node_modules/popper/popper.min.js"></script>
    <script src="../admin-dashboard/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="../admin-dashboard/main/dist/js/perfect-scrollbar.jquery.min.js"></script>
    <!--Wave Effects -->
    <script src="../admin-dashboard/main/dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="../admin-dashboard/main/dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="../admin-dashboard/main/dist/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- This page plugins -->
    <!-- ============================================================== -->
    <!--morris JavaScript -->
    <script src="../admin-dashboard/assets/node_modules/raphael/raphael-min.js"></script>
    <script src="../admin-dashboard/assets/node_modules/morrisjs/morris.min.js"></script>
    <script src="../admin-dashboard/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!--c3 JavaScript -->
    <script src="../admin-dashboard/assets/node_modules/d3/d3.min.js"></script>
    <script src="../admin-dashboard/assets/node_modules/c3-master/c3.min.js"></script>
    <!-- Chart JS -->
    <script src="../admin-dashboard/main/dist/js/dashboard1.js"></script>

    <script>
        $(document).ready(function(){

        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        
        $('#back-to-top').tooltip('show');

        });
    </script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script>
    // $('#add').on("click", function() {
    //     $("#child").append('<li class="list-group"><div class="input-group mb-3"> <div class="input-group-prepend"> <a href = "#" id = "add" class = "btn btn-success input-group-text"><i class="fas fa-minus-circle"></i></a> </div> <select class="form-control"> <option value="" selected>Choose program...</option> @if(count($programs) > 0) @foreach($programs as $program) <option> {{$program->program}} </option> @endforeach @endif </select> </div> </li>');
    // });
    function add(){
        $("#one #two #three").append('<li class="list-group"><div class="input-group mb-3"> <div class="input-group-prepend"> <a href = "#" id = "add" class = "btn btn-success input-group-text"><i class="fas fa-minus-circle"></i></a> </div> <select class="form-control"> <option value="" selected>Choose program...</option> @if(count($programs) > 0) @foreach($programs as $program) <option> {{$program->program}} </option> @endforeach @endif </select> </div> </li>');
    }
</script>

</body>

</html>