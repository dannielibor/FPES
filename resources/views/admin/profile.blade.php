@extends('layouts.admin')

@section('content')
<!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Portal</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark" style = "background-color:maroon">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b style = "margin-right:3%">
                            <img src = "/img/sbca.png" height = "50">
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                        <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Admin Portal</font></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)" style = "color:white"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)" style = "color:white"><i class="fa fa-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-light" href="/pages-profile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/storage/profile_img/{{$admin[0]->img}}" alt="user" class="img-circle" width="30"></a>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                    </ul>
                </div>
            </nav>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src = "/img/sbca.png" height = "50"></span>
                <a class="waves-effect waves-dark ml-auto hidden-sm-down" href="javascript:void(0)" style= "color:white"><i class="ti-menu"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)" style = "color:red"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="{{ action("AdminController@index") }}" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/pages-profile" aria-expanded="false"><i class="fa fa-user-circle-o"></i><span class="hide-menu">Profile</span></a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/users" aria-expanded="false"><i class="fa fa-table"></i><span class="hide-menu"></span>Tables</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/admins" aria-expanded="false"><i class="fas fa-user-tie"></i><span class="hide-menu"></span>Administrators</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="icon-fontawesome.html" aria-expanded="false"><i class="fas fa-list-ul"></i><span class="hide-menu"></span>Forms</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="map-google.html" aria-expanded="false"><i class="fas fa-envelope"></i><span class="hide-menu"></span>Messages</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="pages-blank.html" aria-expanded="false"><i class="fas fa-cog"></i><span class="hide-menu"></span>Settings</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="/trash" aria-expanded="false"><i class="fas fa-trash"></i><span class="hide-menu"></span>Trash</a></li>
                        <li> <a style = "color:white" class="waves-effect waves-dark" href="#" data-toggle="modal" data-target="#logout" aria-expanded="false"><i class="fas fa-sign-out-alt"></i><span class="hide-menu"></span>{{ __('Logout') }}</a></li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
                @include('inc.messages')
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">Profile</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                                <li class="breadcrumb-item active">Profile</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-xlg-3 col-md-5">
                        <div class="card">
                            <div class="card-body">
                                <center class="m-t-30"> <img src="/storage/profile_img/{{$admin[0]->img}}" class="img-circle" width="150" />
                                    <h4 class="card-title m-t-10">{{$admin[0]->firstname}}</h4>
                                    <h6 class="card-subtitle">{{$admin[0]->role}}</h6>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-8 col-xlg-9 col-md-7">
                        <div class="card">
                            <!-- Tab panes -->
                            <div class="card-body">
                                {!! Form::open(['action' => ['AdminProfileController@update', $admin[0]->id], 'method' => 'POST', 'class' => 'form-horizontal form-material']) !!}
                                <div class="form-group">
                                    {{Form::label('firstname', 'Firstname', ['class' => 'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{Form::text('firstname', $admin[0]->firstname, ['class' => 'form-control form-control-line'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{Form::label('lastname', 'Lastname', ['class' => 'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{Form::text('lastname', $admin[0]->lastname, ['class' => 'form-control form-control-line'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {{Form::label('email', 'Email', ['class' => 'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{Form::text('email', $admin[0]->email, ['class' => 'form-control form-control-line'])}}
                                    </div>
                                </div>
                                <div class="form-group">
                                        {{Form::label('username', 'Username', ['class' => 'col-md-12'])}}
                                        <div class="col-md-12">
                                            {{Form::text('username', $admin[0]->username, ['class' => 'form-control form-control-line'])}}
                                        </div>
                                    </div>
                                <div class="form-group">
                                    {{Form::label('password', 'Password', ['class' => 'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{ Form::input('password', 'password', null, ['class' => 'form-control form-control-line']) }}
                                    </div>
                                </div>
                                {{Form::hidden('_method', 'PUT')}}
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button style = "width:10%" type="button" class="btn btn-outline-success" data-toggle = "modal" data-target = "#update">Save changes</button>
                                </div>
                                <!-- update Modal -->
                                <div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="update" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="update">Update</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure to save changes?
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                                {{Form::submit('submit', ['class' => 'btn btn-success'])}}
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                <!-- Row -->
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <footer class="footer">
            © 2018 Elegent Admin by wrappixel.com
        </footer>
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>
    @endsection