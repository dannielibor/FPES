@extends('layouts.app')

<!-- Bootstrap core CSS -->
<link href="slideshow/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="slideshow/css/half-slider.css" rel="stylesheet">

<!-- Bootstrap core CSS -->
<link href="rounded/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="rounded/css/round-about.css" rel="stylesheet">

@section('content')
<section id="home"></section>
<header>
  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <div class = "container carousel-caption" style = "position:absolute;z-index:1;margin-bottom:15%;text-shadow: 2px 2px 10px #000000">
      <h1 style = "margin-bottom:3%;font-size:500%">EVALUATE NOW!</h1>
        <p><a class="btn btn-lg btn-primary" href="{{ route('login') }}" style = "color:white;font-size:150%" data-toggle="modal" data-target="#login">{{ __('Login') }}</a> <a class="btn btn-lg btn-primary" href="{{ route('register') }}" style = "color:white;font-size:150%" data-toggle="modal" data-target="#register">{{ __('Register') }}</a></p>
    </div>
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below-->
      <div class="carousel-item active" style="background-image: url('img/one.jpg')">
        <div class="carousel-caption text-left" style = "text-shadow: 2px 2px 10px #000000">
          <h1>Let’s get started…</h1>
          <p>A way to improve your professor's performance.</p>
          <p><a class="btn btn-lg btn-primary" role="button" href="{{ route('register') }}" style = "color:white;font-size:150%">{{ __('Sign up today') }}</a></p>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('img/two.jpg')">
        <div class="carousel-caption" style = "text-shadow: 2px 2px 10px #000000">
          <h1>How well do you know your professor?</h1>
          <p>“Good teaching is one-fourth preparation and three-fourths theatre.”</p>
          <small>-Gail Goldwin-</small>
          <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('img/three.jpg')">
        <div class="carousel-caption text-right" style = "text-shadow: 2px 2px 3px #000000">
          <h1>See who's on top!</h1>
          <p>“There is no failure.  Only feedback.”</p>
          <small>-Robert Allen-</small>
          <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse achievers</a></p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</header>


      <!-- Marketing messaging and featurettes
      ================================================== -->
      <!-- Wrap the rest of the page in another container to center all the content. -->
      <section id="about"></section>
      <div class="container marketing" style = "margin-top:5%">
          
            <!-- Three columns of text below the carousel -->
            <div class="row text-center">
              <div class="col-lg-4">
                <img class="rounded-circle" src="img/mission.png" alt="Generic placeholder image" width="140" height="140">
                <h2>Mission</h2>
                <p>To provide excellent and responsive programs and services,adopt empowering management system, and build a learning, caring, and praying community guided by the teachings of St. Benedict and the example of St. Bede. </p>
              </div><!-- /.col-lg-4 -->
              <div class="col-lg-4">
                <img class="rounded-circle" src="img/vision.png" alt="Generic placeholder image" width="140" height="140">
                <h2>Vision</h2>
                <p>To be a leading Catholic Christian educational institution committed to the holistic formation of persons who excel in their respective endeavors and are guided by the Benedictine principles of Prayer, Work and Peace. </p>
              </div><!-- /.col-lg-4 -->
              <div class="col-lg-4">
                <img class="rounded-circle" src="img/values.png" alt="Generic placeholder image" width="140" height="140">
                <h2>Core Values</h2>
                <img src="img/prayer.png" alt="Generic placeholder image" width="50" height="50">
                <img src="img/work.png" alt="Generic placeholder image" width="50" height="50">
                <img src="img/peace.png" alt="Generic placeholder image" width="50" height="50">
                <img src="img/community.png" alt="Generic placeholder image" width="50" height="50">
                <img src="img/service.png" alt="Generic placeholder image" width="50" height="50">
              </div><!-- /.col-lg-4 -->
            </div><!-- /.row -->
          
<!-- START THE FEATURETTES -->

<section id="blog"></section>
<hr class="featurette-divider" style = "margin-top:10%">
<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">First featurette heading. <span class="text-muted">It'll blow your mind.</span></h2>
    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
  </div>
  <div class="col-md-5">
    <img class="featurette-image img-fluid mx-auto" src="img/default_2x2.jpg" alt="Generic placeholder image">
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7 order-md-2">
    <h2 class="featurette-heading">Oh yeah, it's that good. <span class="text-muted">See for yourself.</span></h2>
    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
  </div>
  <div class="col-md-5 order-md-1">
    <img class="featurette-image img-fluid mx-auto" src="img/default_2x2.jpg" alt="Generic placeholder image">
  </div>
</div>

<hr class="featurette-divider">

<div class="row featurette">
  <div class="col-md-7">
    <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
    <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
  </div>
  <div class="col-md-5">
    <img class="featurette-image img-fluid mx-auto" src="img/default_2x2.jpg" alt="Generic placeholder image">
  </div>
</div>
<section id="contact"></section>
<hr class="featurette-divider">

<!-- /END THE FEATURETTES -->

</div><!-- /.container -->

<div class="container" style = "margin-top:3%">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Contact us') }}</div>
    
                    <div class="card-body">
                            <form class="form-horizontal" action="/" method="post">
                                    <fieldset>
                              
                                      <!-- Name input-->
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="name">Name</label>
                                        <div class="col-md-9">
                                          <input id="name" name="name" type="text" placeholder="Your name" class="form-control">
                                        </div>
                                      </div>
                              
                                      <!-- Email input-->
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="email">Your E-mail</label>
                                        <div class="col-md-9">
                                          <input id="email" name="email" type="text" placeholder="Your email" class="form-control">
                                        </div>
                                      </div>
                              
                                      <!-- Message body -->
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="message">Your message</label>
                                        <div class="col-md-9">
                                          <textarea class="form-control" id="message" name="message" placeholder="Please enter your message here..." rows="5"></textarea>
                                        </div>
                                      </div>
                              
                                      <!-- Form actions -->
                                      <div class="form-group">
                                        <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary btn-lg">Submit</button>
                                        </div>
                                      </div>
                                    </fieldset>
                                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
    <!-- Login Modal -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="login" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="login">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail or Username') }}</label>

                            <div class="col-md-6">
                                    <input id="identity" type="identity" class="form-control" name="identity"
                                    value="{{ old('identity') }}" autofocus>
           
                             @if ($errors->has('identity'))
                               <span class="help-block">
                                    <strong>{{ $errors->first('identity') }}</strong>
                                </span>
                             @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<!-- Register Modal -->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="register" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
            <form method="POST" action="{{ route('register') }}" class = "was-validated">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="register">Register</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
                

                        @csrf
                
                        <div class="form-row">
                
                                <div class="form-group col-md-6">
                      
                                    <label for="email">{{ __('E-Mail Address') }}</label>
                      
                                    <input placeholder="john@sample.com" id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                
                                    <div class="valid-feedback">
                                            Looks good!
                                    </div>
                
                                    <div class="invalid-feedback">
                                            Please provide a valid email.
                                    </div>
                      
                                </div>
                      
                                <div class="form-group col-md-6">
                      
                                        <label for="username">{{ __('Username') }}</label>
                      
                                        <input placeholder="johndoe" id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required autofocus>
                
                                        @if ($errors->has('username'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                
                                        <div class="valid-feedback">
                                                Looks good!
                                        </div>
                
                                        <div class="invalid-feedback">
                                                Please provide a valid username.
                                        </div>
                      
                                </div>
                      
                              </div>
                
                
                              <div class="form-row">
                
                                    <div class="form-group col-md-6">
                          
                                            <label for="password">{{ __('Password') }}</label>
                          
                                            <input placeholder="Password" id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                
                                            <div class="valid-feedback">
                                                    Looks good!
                                            </div>
                
                                            <div class="invalid-feedback">
                                                    Please provide a valid password.
                                            </div>
                          
                                    </div>
                          
                                    <div class="form-group col-md-6">
                          
                                            <label for="password-confirm">{{ __('Confirm Password') }}</label>
                          
                                            <input placeholder="Confirm Password" id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                
                                                <div class="valid-feedback">
                                                        Looks good!
                                                </div>
                    
                                                <div class="invalid-feedback">
                                                        Please confirm your password.
                                                </div>
                          
                                    </div>
                          
                                  </div>

                                  <div class="form-row">
                
                                        <div class="form-group col-md-6">
                              
                                                <select class="custom-select" name = "role" required>
                                                        <option value="">Choose...</option>
                                                        <option value="student">Student</option>
                                                        <option value="professor">Professor</option>
                                                      </select>
                
                                                      <div class="valid-feedback">
                                                            Looks good!
                                                    </div>
                    
                                                    <div class="invalid-feedback">
                                                            Please indicate your position.
                                                    </div>
                              
                                        </div>


                                        <div class="form-group col-md-6">
                              
                                                <div class="form-check">
                                                        <input class="form-check-input is-invalid" type="checkbox" value="" id="invalidCheck3" required>
                                                        <label class="form-check-label" for="invalidCheck3">
                                                          Agree to terms and conditions
                                                        </label>
                                                      </div>
                              
                                        </div>

                              
                                      </div>

                                      
                     
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">{{ __('Register') }}</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
    </form>
      </div>
    </div>
  </div>

@endsection