        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo" style = "background-color:maroon">
                <a href="#">
                    <img src="img/sbca.png" width = "40" alt="Student"/>
                    <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Student</font>
                </a>
            </div>
            <div class="menu-sidebar__content js-scrollbar1">
                <nav class="navbar-sidebar">
                    <ul class="list-unstyled navbar__list">
                        <li class="active has-sub">
                            <a class="js-arrow" href="/studentdashboard">
                                <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                        </li>
                        <li>
                            <a href="chart.html">
                                <i class="fas fa-chalkboard-teacher"></i>Evaluation</a>
                        </li>
                        <li>
                            <a href="table.html">
                                <i class="far fa-newspaper"></i>News</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>
        <!-- END MENU SIDEBAR-->
        <script>
            $('li > a').click(function() {
                $('li').removeClass();
                $(this).parent().addClass('active');
            });
        </script>