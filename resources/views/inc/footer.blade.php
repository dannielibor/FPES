<!-- Footer -->
<footer class="page-footer font-small mdb-color pt-4 mt-4" style = "font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif">

        <!-- Footer Links -->
        <div class="container text-center text-md-left">
      
          <!-- Footer links -->
          <div class="row text-center text-md-left mt-3 pb-3">
      
            <!-- Grid column -->
            <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                    <img src = "/img/sbca.png" width = "50">
              <p>San Beda College Alabang and is one of the campuses of the San Beda University, a private Roman Catholic university run by the Benedictine monks in the Philippines. It is located in Alabang in Muntinlupa, Metro Manila.</p>
            </div>
            <!-- Grid column -->
      
            <hr class="w-100 clearfix d-md-none">
      
            <!-- Grid column -->
            <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Links</h6>
              <p><a href="#!" style = "text-decoration: none;color:maroon" id = "home">Home</a></p>
              <p><a href="#!" style = "text-decoration: none;color:maroon" id = "about">About</a></p>
              <p><a href="#!" style = "text-decoration: none;color:maroon" id = "blog">Blog</a></p>
              <p><a href="#!" style = "text-decoration: none;color:maroon" id = "contact">Contact Us</a></p>
            </div>
            <!-- Grid column -->
      
            <hr class="w-100 clearfix d-md-none">
      
            <!-- Grid column -->
            <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Services</h6>
              <p><a href="http://www.sanbeda-alabang.edu.ph/bede/" style = "text-decoration: none;color:maroon" target="_blank">Official Website</a></p>
              <p><a href="#!" style = "text-decoration: none;color:maroon" target="_blank">Help</a></p>
            </div>
      
            <!-- Grid column -->
            <hr class="w-100 clearfix d-md-none">
      
            <!-- Grid column -->
            <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
              <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
              <p><i class="fa fa-home mr-3" style = "color:maroon"></i> 8 Don Manolo Blvd, Cupang, Muntinlupa, 1770 Metro Manila</p>
              <p><i class="fa fa-envelope mr-3" style = "color:maroon"></i> sbca@sanbeda-alabang.edu.ph</p>
              <p><i class="fa fa-phone mr-3" style = "color:maroon"></i> 236-7222 </p>
            </div>
            <!-- Grid column -->
      
          </div>
          <!-- Footer links -->
      
          <hr>
      
          <!-- Grid row -->
          <div class="row d-flex align-items-center">
      
            <!-- Grid column -->
            <div class="col-md-8 col-lg-8">
      
              <!--Copyright-->
              <p class="text-center text-md-left">© 2018 Copyright: <a href="http://www.sanbeda-alabang.edu.ph/bede/" style = "text-decoration: none;color:maroon" target="_blank"><strong> San Beda College Alabang</strong></a></p>
      
            </div>
            <!-- Grid column -->
      
            <!-- Grid column -->
            <div class="col-md-4 col-lg-4 ml-lg-0">
      
              <!-- Social buttons -->
              <div class="text-center text-md-right">
                <ul class="list-unstyled list-inline">
                  <li class="list-inline-item"><a class="btn-floating btn-sm rgba-white-slight mx-1" href = "https://www.facebook.com/sbca.cas.sec/" target="_blank"><i class="fab fa-facebook-f" style = "color:maroon"></i></a></li>
                  <li class="list-inline-item"><a class="btn-floating btn-sm rgba-white-slight mx-1" href = "https://twitter.com/sbcasec?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor" target="_blank"><i class="fab fa-twitter" style = "color:maroon"></i></a></li>
                  <li class="list-inline-item"><a class="btn-floating btn-sm rgba-white-slight mx-1" href = "http://www.sanbeda-alabang.edu.ph/bede/" target="_blank"><i class="fab fa-google-plus-g" style = "color:maroon"></i></a></li>
                  <li class="list-inline-item"><a class="btn-floating btn-sm rgba-white-slight mx-1" href = "http://www.sanbeda-alabang.edu.ph/bede/" target="_blank"><i class="fab fa-linkedin-in" style = "color:maroon"></i></a></li>
                </ul>
              </div>
      
            </div>
            <!-- Grid column -->
      
          </div>
          <!-- Grid row -->
      
        </div>
        <!-- Footer Links -->

        <script>
            $(document).ready(function(){

            $('#home').click(function () {
                $('#home').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });

            $('#about').click(function () {
                $('#about').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 500
                }, 800);
                return false;
            });

            $('#blog').click(function () {
                $('#blog').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 1070
                }, 800);
                return false;
            });

            $('#contact').click(function () {
                $('#contact').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 2500
                }, 800);
                return false;
            });
    
    });
        </script>
      
      </footer>
      <!-- Footer -->