<nav class="navbar navbar-expand-md navbar-dark fixed-top navbar-laravel" style = "background-color:maroon">

        <a class="navbar-brand" href="/home">
            <img src = "img/sbca.png" height = "50" style = "margin-right:3%">
            <font face="Old English Text MT" size="6" color = "#ffffff" class = "title" style="cursor:default">Admin Portal</font>
        </a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">

    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">

    </div>

</nav>

<!-- Logout Modal -->
<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="logout" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="logout">Logout</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              Are you sure?
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-danger" onclick="event.preventDefault();
              document.getElementById('logout-form').submit();">Continue</button>
            </div>
          </div>
        </div>
      </div>