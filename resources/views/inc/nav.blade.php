<nav class="col-md-2 d-none d-md-block bg-light sidebar" style = "padding-top:2.5%;border-right:1px solid gray">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="/dashboard" style = "color:black">
                  <i class="fas fa-home"></i>
                Dashboard <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" style = "color:black">
                  <i class="fas fa-file"></i>
                Forms
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" style = "color:black">
                  <i class="fas fa-th-large"></i>
                Blogs
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/users" style = "color:black">
                  <i class="fas fa-users"></i>
                Users
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#" style = "color:black">
                  <i class="fas fa-chart-bar"></i>
                Reports
              </a>
            </li>
            <li class="nav-item text-nowrap">
                    <a class="nav-link" href="#" data-toggle="modal" data-target="#logout">
                            {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
          </ul>
        </div>
      </nav>
      