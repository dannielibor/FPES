<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    // Table Name
    protected $table = 'students';
    // Foreign Key
    public $foreignKey = 'userId';
    // Timestamps
    public $timestamps = true;

}
