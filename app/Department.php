<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    // Table Name
    protected $table = 'departments';
    // Foreign Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;

    public function programs(){
        return $this->hasMany('App\Program', 'deptId', 'id');
    }
}
