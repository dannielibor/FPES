<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cor extends Model
{
    // Table Name
    protected $table = 'cors';
    // Foreign Key
    public $primaryKey = 'regId';
    // Timestamps
    public $timestamps = true;

    public function sched(){
        return $this->hasMany('App\Schedule', 'regId');
    }
}
