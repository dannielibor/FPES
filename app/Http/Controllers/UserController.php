<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\userBin;
use App\Student;
use App\Professor;
use DB;
use App\cor;
use Excel;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use App\Schedule;
use App\Program;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = User::where('role', 'admin')->get();
        $user = User::where('role', '!=', 'admin')->orderBy('created_at', 'desc')->paginate(5);
        $cor = cor::paginate(5);
        return view('admin.users')->with('users', $user)->with('cors', $cor)->with('admin', $admin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'profile_img' => 'image|nullable|max:1999'
        ]);

        $user = new User;

        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->isAdmin = '0';
        $user->role = $request->role;

        if($request->hasFile('profile_img')){
            // get file name with the extension
            $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
            // get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // get just ext
            $extension = $request->file('profile_img')->getClientOriginalExtension();
            // filename to store
            $filenameToStore = $filename.'_'.time().'.'.$extension;
            // upload image
            $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
            
            $user->img = $filenameToStore;
        }

        if($request->firstname){

            $user->firstname = $request->firstname;

        }

        if($request->lastname){

            $user->lastname = $request->lastname;

        }

        if($request->role == 'student'){

            $student = new Student;

            if($request->studNum){

                $cor = cor::where('student_id', '=', $request->studNum)->get();

                if(count($cor) < 1){

                    return back()->with('error', "Student ID doesn't exist");

                }else{

                    $user->status = 'activated';
                    $user->save();

                    $user = User::where('username', '=', $request->username)->get();

                    $student->student_id = $request->studNum;
                    $student->userId = $user[0]->id;
                    $student->save();

                    return back()->with('success', 'Successfully created');

                }

            }else{

                $user->status = 'pending';
                $user->save();

                $user = User::where('username', '=', $request->username)->get();

                $student->userId = $user[0]->id;
                $student->save();

                return back()->with('success', 'Successfully created');

            }

        }else if($request->role == 'professor'){

            $professor = new Professor;

            if($request->profNum){

                $sched = Schedule::where('facultyId', '=', $request->profNum)->get();

                if(count($sched) < 1){

                    return back()->with('error', "Student ID doesn't exist");

                }else{

                    $user->status = 'activated';
                    $user->save();

                    $user = User::where('username', '=', $request->username)->get();

                    $professor->faculty_id = $request->profNum;
                    $professor->userId = $user[0]->id;
                    $professor->save();

                    return back()->with('success', 'Successfully created');

                }

            }else{

                $user->status = 'pending';
                $user->save();

                $user = User::where('username', '=', $request->username)->get();

                $professor->userId = $user[0]->id;
                $professor->save();

                return back()->with('success', 'Successfully created');

            }

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        return $id;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = User::where('role', 'admin')->get();
        $user = User::find($id);
        return view('admin.showUser')->with('user', $user)->with('admin', $admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'profile_img' => 'image|nullable|max:1999'
        ]);

        $user = User::find($id);
        $student = Student::where('userId', '=', $id)->first();
        $professor = Professor::where('userId', '=', $id)->first();

        if($user->role == 'student'){

            $user->firstname = $request->input('firstname');
            $user->lastname = $request->input('lastname');
            $user->email = $request->input('email');
            $user->username = $request->input('username');

            if($request->input('password')){

                $user->password = Hash::make($request->input('password'));

            }

            $cor = cor::where('student_id', '=', $request->input('studNum'))->get();

            if(count($cor) < 1 && $request->input('studNum') != null){

                return back()->with('error', "Student ID doesn't exist");

            }else if(count($cor) > 0){

                $student->student_id = $request->input('studNum');
                $user->status = 'activated';
                if($request->hasFile('profile_img')){
                    if($request->image != 'default.jpg'){
                        Storage::delete('public/profile_img/'.$user->img);
                    }
                    // get file name with the extension
                    $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
                    // get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // get just ext
                    $extension = $request->file('profile_img')->getClientOriginalExtension();
                    // filename to store
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    // upload image
                    $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
                    $user->img = $filenameToStore;
                }
                $user->save();
                $student->save();

                return back()->with('success', "Update successfully");

            }else{

                $student->student_id = $request->input('studNum');
                $user->status = 'pending';
                if($request->hasFile('profile_img')){
                    if($request->image != 'default.jpg'){
                        Storage::delete('public/profile_img/'.$user->img);
                    }
                    // get file name with the extension
                    $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
                    // get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // get just ext
                    $extension = $request->file('profile_img')->getClientOriginalExtension();
                    // filename to store
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    // upload image
                    $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
                    $user->img = $filenameToStore;
                }
                $user->save();
                $student->save();

                return back()->with('success', "Update successfully");

            }

        }else if ($user->role == 'professor'){

            $user->firstname = $request->input('firstname');
            $user->lastname = $request->input('lastname');
            $user->email = $request->input('email');
            $user->username = $request->input('username');

            if($request->input('password')){

                $user->password = Hash::make($request->input('password'));

            }

            $sched = Schedule::where('facultyId', $request->input('profNum'))->get();
            
            if(count($sched) < 1 && $request->input('profNum') != null){

                return back()->with('error', "Faculty ID doesn't exist");

            }else if(count($sched) > 0){

                $professor->faculty_id = $request->input('profNum');
                $user->status = 'activated';
                if($request->hasFile('profile_img')){
                    if($request->image != 'default.jpg'){
                        Storage::delete('public/profile_img/'.$user->img);
                    }
                    // get file name with the extension
                    $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
                    // get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // get just ext
                    $extension = $request->file('profile_img')->getClientOriginalExtension();
                    // filename to store
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    // upload image
                    $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
                    $user->img = $filenameToStore;
                }
                $user->save();
                $professor->save();

                return back()->with('success', "Update successfully");

            }else{

                $professor->faculty_id = $request->input('profNum');
                $user->status = 'pending';
                if($request->hasFile('profile_img')){
                    if($request->image != 'default.jpg'){
                        Storage::delete('public/profile_img/'.$user->img);
                    }
                    // get file name with the extension
                    $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
                    // get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // get just ext
                    $extension = $request->file('profile_img')->getClientOriginalExtension();
                    // filename to store
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    // upload image
                    $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
                    $user->img = $filenameToStore;
                }
                $user->save();
                $professor->save();

                return back()->with('success', "Update successfully");

            }

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $user = User::find($id);

        $bin = new userBin;

        $bin->userId = $user->id;
        $bin->role = $user->role;
        $bin->isAdmin = $user->isAdmin;
        $bin->firstname = $user->firstname;
        $bin->lastname = $user->lastname;
        $bin->email = $user->email;
        $bin->username = $user->username;
        $bin->password = $user->password;
        $bin->status = $user->status;
        $bin->img = $user->img;
        
        if($user->role == 'student'){

            $student = Student::where('userId', $id)->get();

            $bin->idNumber = $student[0]->student_id;

            $bin->save();
    
            $user->delete();
            $student = Student::where('userId', $id)->delete();

        }else if ($user->role == 'professor'){

            $professor = Professor::find($id);

            $bin->idNumber = $student->student_id;
    
            $bin->save();
    
            $user->delete();
            $professor->delete();

        }

        return back()->with('success', 'User has been moved to trash');

    }
    public function search(Request $request){
        $admin = User::where('role', 'admin')->get();
        $user = User::select(DB::Raw('*'))
                ->join('students', 'users.id', '=', 'students.userId')
                ->where('students.student_id', '=', $request->search)
                ->orWhere('users.id', 'like', '%'.$request->search.'%')
                ->orWhere('users.firstname', 'like', '%'.$request->search.'%')
                ->orWhere('users.lastname', 'like', '%'.$request->search.'%')
                ->orWhere('users.email', 'like', '%'.$request->search.'%')
                ->orWhere('users.username', 'like', '%'.$request->search.'%')
                ->orWhere('users.role', 'like', '%'.$request->search.'%')
                ->orWhere('users.status', 'like', '%'.$request->search.'%')
                ->orWhere('users.created_at', 'like', '%'.$request->search.'%')
                ->orWhere('users.updated_at', 'like', '%'.$request->search.'%')->paginate(5);

        $cor = cor::where('regId', 'like', '%'.$request->search.'%')
                ->orWhere('student_id', 'like', '%'.$request->search.'%')
                ->orWhere('firstname', 'like', '%'.$request->search.'%')
                ->orWhere('lastname', 'like', '%'.$request->search.'%')
                ->orWhere('created_at', 'like', '%'.$request->search.'%')
                ->orWhere('updated_at', 'like', '%'.$request->search.'%')->paginate(5);

        return view('admin.users')->with('users', $user)->with('cors', $cor)->with('admin', $admin);
    }
    public function import(Request $request){

        if($request->hasFile('file')){

            $path = $request->file('file')->getRealPath();
            $data = Excel::load($path, function($reader){})->get();
            $error = 0;

                if(!empty($data) && $data->count()){

                    foreach($data as $key => $value){

                        $check = cor::where('regId', $value->reg_id)->get();

                        if(count($check) < 1){

                        $cor = new cor();

                        $cor->regId = $value->reg_id;
                        $cor->student_id = $value->student_id;
                        $cor->firstname = $value->firstname;
                        $cor->lastname = $value->lastname;
                        $cor->gender = $value->gender;
                        $cor->program = $value->program;
                        $cor->yearLvl = $value->yearlvl;
                        $cor->academic_year = $value->academic_year;
                        $cor->term = $value->term;

                        $sched = new Schedule();

                        $sched->regId = $value->reg_id;
                        $sched->facultyId = $value->facultyid;
                        $sched->code = $value->code;
                        $sched->subject_title = $value->subject_title;
                        $sched->section = $value->section;
                        $sched->date = $value->date;
                        $sched->time = $value->time;
                        $sched->room = $value->room;

                        $sched->save();
                        $cor->save();

                        }else{
                            $error += 1;
                        }

                    }

                    $program = cor::distinct()->get(['program']);

                    foreach($program as $value){

                        $check = Program::where('program', $value->program)->get();

                        if(count($check) < 1){

                        $programs = new Program();

                        $programs->program = $value->program;
                        $programs->status = 'unassigned';
                        
                        $programs->save();

                        }

                    }

                }

        }

        if($error > 0){
            return back()->with('error', $error.' rows already existed');
        }else{
            return back();
        }

    }
    public function deleteAll(Request $request){

        $del = $request->input('delete');

        if($del != ''){

            for($x = 0; $x < count($del); $x++){

                $user = User::find($del[$x]);
    
                $bin = new userBin;
    
                $bin->userId = $user->id;
                $bin->role = $user->role;
                $bin->isAdmin = $user->isAdmin;
                $bin->firstname = $user->firstname;
                $bin->lastname = $user->lastname;
                $bin->email = $user->email;
                $bin->username = $user->username;
                $bin->password = $user->password;
                $bin->status = $user->status;
                $bin->img = $user->img;
                
                if($user->role == 'student'){
    
                    $student = Student::where('userId', $del[$x])->get();
    
                    $bin->idNumber = $student[0]->student_id;
    
                    $bin->save();
            
                    $user->delete();
                    $student = Student::where('userId', $del[$x])->delete();
    
                }else if ($user->role == 'professor'){
    
                    $professor = Professor::find($del[$x]);
    
                    $bin->idNumber = $student->student_id;
            
                    $bin->save();
            
                    $user->delete();
                    $professor->delete();
    
                }
    
            }

            return back()->with('success', 'User has been moved to trash');

        }else{
            return back()->with('error', 'Nothing to delete');
        }

        

        
        
    }
}
