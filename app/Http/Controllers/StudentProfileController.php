<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Student;
use App\cor;
use Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class StudentProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = User::find(auth()->user()->id);
        return view('student.profile')->with('student', $student);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'profile_img' => 'image|nullable|max:1999'
        ]);

        $user = User::find($id);
        $student = Student::where('userId', '=', $id)->first();

            $user->firstname = $request->input('firstname');
            $user->lastname = $request->input('lastname');
            $user->email = $request->input('email');
            $user->username = $request->input('username');

            if($request->input('password')){

                $user->password = Hash::make($request->input('password'));

            }

            $cor = cor::where('student_id', '=', $request->input('studNum'))->get();

            if(count($cor) < 1 && $request->input('studNum') != null){

                return back()->with('error', "Student ID doesn't exist");

            }else if(count($cor) > 0){

                $student->student_id = $request->input('studNum');
                $user->status = 'activated';
                if($request->hasFile('profile_img')){
                    if($request->image != 'default.jpg'){
                        Storage::delete('public/profile_img/'.$user->img);
                    }
                    // get file name with the extension
                    $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
                    // get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // get just ext
                    $extension = $request->file('profile_img')->getClientOriginalExtension();
                    // filename to store
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    // upload image
                    $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
                    $user->img = $filenameToStore;
                }
                $user->save();
                $student->save();

                return back()->with('success', "Update successfully");

            }else{

                $student->student_id = $request->input('studNum');
                $user->status = 'pending';
                if($request->hasFile('profile_img')){
                    if($request->image != 'default.jpg'){
                        Storage::delete('public/profile_img/'.$user->img);
                    }
                    // get file name with the extension
                    $filenameWithExt = $request->file('profile_img')->getClientOriginalName();
                    // get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // get just ext
                    $extension = $request->file('profile_img')->getClientOriginalExtension();
                    // filename to store
                    $filenameToStore = $filename.'_'.time().'.'.$extension;
                    // upload image
                    $path = $request->file('profile_img')->storeAs('public/profile_img', $filenameToStore);
                    $user->img = $filenameToStore;
                }
                $user->save();
                $student->save();

                return back()->with('success', "Update successfully");

            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function home(){
        $user = User::find(auth()->user()->id);
        return view('student.dashboard')->with('student', $user);
    }
}
