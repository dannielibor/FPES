<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Program;
use App\Department;
use App\Student;

class AdminController extends Controller
{

    private $department_id;
    
    public function login(){
        return view('admin/login');
    }
    public function index(){
        $admin = User::where('role', 'admin')->get();
        return view('admin.dashboard')->with('admin', $admin);
    }
    public function admins(){
        $program = Program::where('status', 'unassigned')->get();
        $department = Department::all();
        $admin = User::where('role', 'admin')->get(); 
        return view('admin.admins')->with('admin', $admin)->with('programs', $program)->with('departments', $department);
    }
    public function store(Request $request){
        // $data = request('array');
        // $data = json_decode($data);
        // foreach ($data as $key => $value) {
        //     echo $value->id.'<br>';
        //     // $department = Department::where('department', $value->id)->get();
        //     // if(count($department) == 0){
        //     //     $department = new Department();
        //     //     $department->department = $value->id;
        //     //     $department->save();
        //     // }
        //     // $deptId = Department::where('department', $value->id)->first();
        //     // $this->department_id = $deptId->id;
        //     foreach ($value as $subkey => $subvalue) {
        //         if (is_array($subvalue)) { 
        //             foreach ($subvalue as $subsubkey => $subsubvalue) {
        //                 echo 'CHILD: '. $subsubvalue->id.'<br>';
        //                 // $program = Program::where('program', $subsubvalue->id)->first();
        //                 // $program->deptId = $this->department_id;
        //                 // $program->status = 'assigned';
        //                 // $program->save();
        //             }
        //         }
        //     }
        // }
        // // return back();

        echo $request->array;
    }
    public function add(Request $request){
        $department = new Department;
        $department->department = $request->department;
        $department->save(); 
        return back()->with('success', 'Department successfully added');
    }
}
