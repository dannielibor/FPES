<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\userBin;
use App\User;
use App\Student;

class TrashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin = User::where('role', 'admin')->get();
        $bin = userBin::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.trash')->with('bin', $bin)->with('admin', $admin);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = User::where('role', 'admin')->get();
        $bin = userBin::where('userId', '=', $id)->get();
        return view('admin.viewTrash')->with('bin', $bin)->with('admin', $admin);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return 123;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        userBin::where('userId', '=', $id)->delete();
        $admin = User::where('role', 'admin')->get();
        $bin = userBin::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.trash')->with('bin', $bin)->with('admin', $admin)->with('success', 'User has been permanently deleted');
    }

    public function search(Request $request){

        $admin = User::where('role', 'admin')->get();

        $bin = userBin::where('userId', 'like', '%'.$request->search.'%')
                ->orWhere('idNumber', 'like', '%'.$request->search.'%')
                ->orWhere('firstname', 'like', '%'.$request->search.'%')
                ->orWhere('lastname', 'like', '%'.$request->search.'%')
                ->orWhere('email', 'like', '%'.$request->search.'%')
                ->orWhere('username', 'like', '%'.$request->search.'%')
                ->orWhere('role', 'like', '%'.$request->search.'%')
                ->orWhere('status', 'like', '%'.$request->search.'%')
                ->orderBy('created_at', 'desc')->paginate(5);

        return view('admin.trash')->with('bin', $bin)->with('admin', $admin);
    }

    public function deleteAll(Request $request){
        
        if($request->submit == 'recover'){

            $bin = userBin::whereIn('userId', $request->input('delete'))->get();
            $user = new User;
            $student = new Student;

            for($x = 0; $x < count($request->input('delete')); $x++){

                $user->id = $bin[$x]->userId;
                $user->firstname = $bin[$x]->firstname;
                $user->lastname = $bin[$x]->lastname;
                $user->email = $bin[$x]->email;
                $user->username = $bin[$x]->username;
                $user->password = $bin[$x]->password;
                $user->isAdmin = $bin[$x]->isAdmin;
                $user->role = $bin[$x]->role;
                $user->status = $bin[$x]->status;
                $user->img = $bin[$x]->img;

                $user->save();

                $user = User::find($bin[$x]->userId);

                $student->userId = $user->id;
                $student->student_id = $bin[$x]->idNumber;

                $student->save();

                userBin::where('userId', $bin[$x]->userId)->delete();

            }

            return back()->with('success', 'User recovered successfully');

        }else if ($request->submit == 'delete'){

            $del = $request->input('delete');

            userBin::whereIn('userId', $del)->delete();

            return back()->with('success', 'User has been permanently deleted');

        }

    }
}
