<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Auth;
use App\Student;
use App\Professor;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $admin = User::where('role', 'admin')->get();

        if(auth()->user()->isAdmin == 1){
            if(URL::previous() == "http://127.0.0.1:8000/admin"){
                return view('admin.dashboard')->with('admin', $admin); 
            }else{
                Auth::logout();
                return redirect()->back()->with('error', 'You must login to the admin portal');
            }
        }else if(auth()->user()->isAdmin == 0){
            if(URL::previous() == "http://127.0.0.1:8000/admin"){
                Auth::logout();
                return redirect()->back()->with('error', 'You must login as admin');
            }else{
                if(auth()->user()->role == "student"){
                    $id = auth()->user()->id;
                    $student = Student::where('userId', $id)->get();
                    if(count($student) == 0){
                        $student = new Student;
                        $student->userId = $id;
                        $student->save();
                    }
                    $user = User::find($id);
                    return view('student/dashboard')->with('student', $user);
                }else if (auth()->user()->role == "professor"){
                    $id = auth()->user()->id;
                    $professor = Professor::where('userId', $id)->get();
                    if(count($professor) == 0){
                        $professor = new Professor;
                        $professor->userId = $id;
                        $professor->save();
                    }
                    $user = User::find($id);
                    return view('professor/profile')->with('prof', $user);
                }
            }
        }
    }

    public function admin()
    { 
        return view('admin/dashboard');
    }
}
