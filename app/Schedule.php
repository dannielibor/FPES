<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    // Table Name
    protected $table = 'schedules';
    // Foreign Key
    public $foreignKey = 'regId';
    // Timestamps
    public $timestamps = true;

    protected $fillable = [
        'regId', 'facultyId', 'code', 'subject_title', 'section', 'date', 'time', 'room'
    ];

}
