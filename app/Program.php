<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    // Table Name
    protected $table = 'programs';
    // Primary Key
    public $primaryKey = 'id';
    // Timestamps
    public $timestamps = true;
    
    public function department(){
        return $this->belongsTo('App\Department', 'deptId', 'id');
    }
}
