<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Professor extends Model
{
    // Table Name
    protected $table = 'professors';
    // Foreign Key
    public $foreignKey = 'userId';
    // Timestamps
    public $timestamps = true;

    public function users(){
        return $this->hasOne('App\User', 'id');
    }

    public function schedules(){
        return $this->hasMany('App\Schedule', 'facultyId');
    }

}
