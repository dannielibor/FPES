<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

// HomeController
Route::get('/home', 'HomeController@index');

// UserController
Route::resource('users', 'UserController')->middleware('admin');
Route::get('/search', 'UserController@search')->middleware('admin');
Route::post('/create', 'UserController@create')->middleware('admin');
Route::post('/import', 'UserController@import')->middleware('admin');
Route::get('/delete/{id}', 'UserController@destroy')->middleware('admin');
Route::post('/deleteAll', 'UserController@deleteAll')->middleware('admin');

// AdminController
Route::get('/admins', 'AdminController@admins');
Route::get('/admin', 'AdminController@login');
Route::get('/dashboard', 'AdminController@index')->middleware('admin');

// AdminProfileController
Route::resource('pages-profile', 'AdminProfileController')->middleware('admin');

// StudentController
Route::resource('student-dashboard', 'StudentController');
Route::get('/student-list', 'StudentController@show');

// StudentProfileController
Route::resource('student-profile', 'StudentProfileController');
Route::get('/studentdashboard', 'StudentProfileController@home')->middleware('auth');

// TrashController
Route::get('/searchTrash', 'TrashController@search')->middleware('admin');
Route::resource('trash', 'TrashController')->middleware('admin');
Route::post('/trashAll', 'TrashController@deleteAll')->middleware('admin');
Route::get('/deleteTrash/{id}', 'TrashController@destroy')->middleware('admin');
Route::get('/view/{id}', 'TrashController@show')->middleware('admin');

Route::get('/departmentsubmit', 'AdminController@store');
Route::get('/department', 'AdminController@add');

Route::get('/trash_department', 'AdminController@trash');